package moe.txuritan.pendulum.client;

import moe.txuritan.pendulum.common.Reference;
import moe.txuritan.pendulum.common.items.copper.Copper;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;

public class CreativeTab {
	public static final CreativeTabs tabPendulum = new CreativeTabs(Reference.Info.MODID + "." + "pendulum") {
		@Override
		public Item getTabIconItem() {
			return Copper.ingotCopper;
		}
	};
}
