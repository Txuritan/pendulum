package moe.txuritan.pendulum.common;

import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.SidedProxy;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import cpw.mods.fml.common.event.FMLPostInitializationEvent;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;
import cpw.mods.fml.common.network.simpleimpl.SimpleNetworkWrapper;

import java.util.logging.Logger;

@Mod(modid = Reference.Info.MODID, name = Reference.Info.MODNAME, version = Reference.Info.VERSION, dependencies = Reference.Info.DEPENDENCIES)
public class Pendulum {
	@Mod.Instance(Reference.Info.MODID)
	public static Pendulum instance;

	public static final Logger logger = Logger.getLogger("Pendulum");

	@SidedProxy(clientSide = Reference.Info.CLIENTPROXY, serverSide = Reference.Info.COMMONPROXY)
	public static CommonProxy proxy;

	public static final String networkChannelName = "Pendulum";
	public static SimpleNetworkWrapper network;

	@Mod.EventHandler
	public void preInit(FMLPreInitializationEvent event) {
		proxy.preInit(event);
	}

	@Mod.EventHandler
	public void init(FMLInitializationEvent event) {
		proxy.init(event);
	}

	@Mod.EventHandler
	public void postInit(FMLPostInitializationEvent event) {
		proxy.postInit(event);
	}
}
