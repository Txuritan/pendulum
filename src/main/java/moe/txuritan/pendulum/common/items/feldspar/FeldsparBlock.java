package moe.txuritan.pendulum.common.items.feldspar;

import moe.txuritan.pendulum.client.CreativeTab;
import moe.txuritan.pendulum.common.Reference;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.creativetab.CreativeTabs;

public class FeldsparBlock extends Block {
	public FeldsparBlock(Material material) {
		super(material);
		setCreativeTab(CreativeTab.tabPendulum);
		setBlockName("blockFeldspar");
		setBlockTextureName(Reference.Info.MODID + ":" + getUnlocalizedName());
	}
}
