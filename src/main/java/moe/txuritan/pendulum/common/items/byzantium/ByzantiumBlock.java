package moe.txuritan.pendulum.common.items.byzantium;

import moe.txuritan.pendulum.client.CreativeTab;
import moe.txuritan.pendulum.common.Reference;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.creativetab.CreativeTabs;

public class ByzantiumBlock extends Block {
	public ByzantiumBlock(Material material) {
		super(material);
		setCreativeTab(CreativeTab.tabPendulum);
		setBlockName("blockByzantium");
		setBlockTextureName(Reference.Info.MODID + ":" + getUnlocalizedName());
	}
}
