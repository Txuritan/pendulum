package moe.txuritan.pendulum.common.items.altarus;

import cpw.mods.fml.common.registry.GameRegistry;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.item.Item;

public class Altarus {
	public static Block blockAltarus = new AltarusBlock(Material.iron);
	public static Item ingotAltarus = new AltarusIngot();
	public static Block oreAltarus = new AltarusOre(Material.rock);

	public static void altarus() {
		GameRegistry.registerBlock(blockAltarus, "blockAltarus");
		GameRegistry.registerItem(ingotAltarus, "ingotAltarus");
		GameRegistry.registerBlock(oreAltarus, "oreAltarus");
	}
}
