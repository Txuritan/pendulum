package moe.txuritan.pendulum.common.items.adlourite;

import cpw.mods.fml.common.registry.GameRegistry;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.item.Item;

public class Adlourite {
	public static Block blockAdlourite = new AdlouriteBlock(Material.iron);
	public static Item ingotAdlourite = new AdlouriteIngot();
	public static Block oreAdlourite = new AdlouriteOre(Material.rock);
	
	public static void adlourite() {
		GameRegistry.registerBlock(blockAdlourite, "blockAdlourite");
		GameRegistry.registerItem(ingotAdlourite, "ingotAdlourite");
		GameRegistry.registerBlock(oreAdlourite, "oreAdlourite");
	}
}
