package moe.txuritan.pendulum.common.items.feminium;

import moe.txuritan.pendulum.client.CreativeTab;
import moe.txuritan.pendulum.common.Reference;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.creativetab.CreativeTabs;

public class FeminiumBlock extends Block {
	public FeminiumBlock(Material material) {
		super(material);
		setCreativeTab(CreativeTab.tabPendulum);
		setBlockName("blockFeminium");
		setBlockTextureName(Reference.Info.MODID + ":" + getUnlocalizedName());
	}
}
