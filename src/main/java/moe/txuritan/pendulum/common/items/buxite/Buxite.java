package moe.txuritan.pendulum.common.items.buxite;

import cpw.mods.fml.common.registry.GameRegistry;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.item.Item;

public class Buxite {
	public static Block blockBuxite = new BuxiteBlock(Material.iron);
	public static Item ingotBuxite = new BuxiteIngot();
	public static Block oreBuxite = new BuxiteOre(Material.rock);

	public static void buxite() {
		GameRegistry.registerBlock(blockBuxite, "blockBuxite");
		GameRegistry.registerItem(ingotBuxite, "ingotBuxite");
		GameRegistry.registerBlock(oreBuxite, "oreBuxite");
	}
}
