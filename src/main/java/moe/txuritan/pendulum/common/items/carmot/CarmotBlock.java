package moe.txuritan.pendulum.common.items.carmot;

import moe.txuritan.pendulum.client.CreativeTab;
import moe.txuritan.pendulum.common.Reference;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.creativetab.CreativeTabs;

public class CarmotBlock extends Block {
	public CarmotBlock(Material material) {
		super(material);
		setCreativeTab(CreativeTab.tabPendulum);
		setBlockName("blockCarmot");
		setBlockTextureName(Reference.Info.MODID + ":" + getUnlocalizedName());
	}
}
