package moe.txuritan.pendulum.common.items.destronium;

import moe.txuritan.pendulum.client.CreativeTab;
import moe.txuritan.pendulum.common.Reference;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.creativetab.CreativeTabs;

public class DestroniumBlock extends Block {
	public DestroniumBlock(Material material) {
		super(material);
		setCreativeTab(CreativeTab.tabPendulum);
		setBlockName("blockDestronium");
		setBlockTextureName(Reference.Info.MODID + ":" + getUnlocalizedName());
	}
}
