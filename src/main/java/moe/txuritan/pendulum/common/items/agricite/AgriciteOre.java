package moe.txuritan.pendulum.common.items.agricite;

import moe.txuritan.pendulum.client.CreativeTab;
import moe.txuritan.pendulum.common.Reference;
import moe.txuritan.pendulum.common.util.StringHelper;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;

import java.util.List;

public class AgriciteOre extends Block {
	public AgriciteOre(Material material) {
		super(material);
		setCreativeTab(CreativeTab.tabPendulum);
		setBlockName("oreAgricite");
		setBlockTextureName(Reference.Info.MODID + ":" + getUnlocalizedName());
	}

	public void addInformation(ItemStack stack, EntityPlayer player, List list, boolean par4) {
		if (StringHelper.displayShiftForDetail && !StringHelper.isShiftKeyDown()) {
			list.add(StringHelper.shiftForDetails());
		}
		if (!StringHelper.isShiftKeyDown()) {
			return;
		}
		list.add(StringHelper.getInfoText("info.pendulum.mod.name"));
		list.add(StringHelper.getInfoText("info.pendulum.ore.agricite.rarity."));
		list.add(StringHelper.getInfoText("info.pendulum.ore.agricite.dim."));
	}
}