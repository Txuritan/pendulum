package moe.txuritan.pendulum.common.items.astralsilver;

import moe.txuritan.pendulum.client.CreativeTab;
import moe.txuritan.pendulum.common.Reference;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.creativetab.CreativeTabs;

public class AstralSilverBlock extends Block {
	public AstralSilverBlock(Material material) {
		super(material);
		setCreativeTab(CreativeTab.tabPendulum);
		setBlockName("blockAstralSilver");
		setBlockTextureName(Reference.Info.MODID + ":" + getUnlocalizedName());
	}
}
