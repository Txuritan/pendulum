package moe.txuritan.pendulum.common.items.caesiumflankolithicmixialubidiumrixidixidexidoxidroxide;

import moe.txuritan.pendulum.client.CreativeTab;
import moe.txuritan.pendulum.common.Reference;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.creativetab.CreativeTabs;

public class CaesiumflankolithicmixialubidiumrixidixidexidoxidroxideBlock extends Block {
	public CaesiumflankolithicmixialubidiumrixidixidexidoxidroxideBlock(Material material) {
		super(material);
		setCreativeTab(CreativeTab.tabPendulum);
		setBlockName("blockCaesiumflankolithicmixialubidiumrixidixidexidoxidroxide");
		setBlockTextureName(Reference.Info.MODID + ":" + getUnlocalizedName());
	}
}
