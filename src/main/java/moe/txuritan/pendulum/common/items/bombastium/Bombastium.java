package moe.txuritan.pendulum.common.items.bombastium;

import cpw.mods.fml.common.registry.GameRegistry;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.item.Item;

public class Bombastium {
	public static Block blockBombastium = new BombastiumBlock(Material.iron);
	public static Item ingotBombastium = new BombastiumIngot();
	public static Block oreBombastium = new BombastiumOre(Material.rock);

	public static void bombastium() {
		GameRegistry.registerBlock(blockBombastium, "blockBombastium");
		GameRegistry.registerItem(ingotBombastium, "ingotBombastium");
		GameRegistry.registerBlock(oreBombastium, "oreBombastium");
	}
}
