package moe.txuritan.pendulum.common.items.astmospherium;

import moe.txuritan.pendulum.client.CreativeTab;
import moe.txuritan.pendulum.common.Reference;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.creativetab.CreativeTabs;

public class AstmospheriumBlock extends Block {
	public AstmospheriumBlock(Material material) {
		super(material);
		setCreativeTab(CreativeTab.tabPendulum);
		setBlockName("blockAstmospherium");
		setBlockTextureName(Reference.Info.MODID + ":" + getUnlocalizedName());
	}
}
