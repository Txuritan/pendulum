package moe.txuritan.pendulum.common.items.disgruntium;

import cpw.mods.fml.common.registry.GameRegistry;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.item.Item;

public class Disgruntium {
	public static Block blockDisgruntium = new DisgruntiumBlock(Material.iron);
	public static Item ingotDisgruntium = new DisgruntiumIngot();
	public static Block oreDisgruntium = new DisgruntiumOre(Material.rock);

	public static void disgruntium() {
		GameRegistry.registerBlock(blockDisgruntium, "blockDisgruntium");
		GameRegistry.registerItem(ingotDisgruntium, "ingotDisgruntium");
		GameRegistry.registerBlock(oreDisgruntium, "oreDisgruntium");
	}
}
