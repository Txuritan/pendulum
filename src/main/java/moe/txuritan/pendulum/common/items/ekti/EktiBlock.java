package moe.txuritan.pendulum.common.items.ekti;

import moe.txuritan.pendulum.client.CreativeTab;
import moe.txuritan.pendulum.common.Reference;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.creativetab.CreativeTabs;

public class EktiBlock extends Block {
	public EktiBlock(Material material) {
		super(material);
		setCreativeTab(CreativeTab.tabPendulum);
		setBlockName("blockEkti");
		setBlockTextureName(Reference.Info.MODID + ":" + getUnlocalizedName());
	}
}
