package moe.txuritan.pendulum.common.items.cinnabar;

import moe.txuritan.pendulum.client.CreativeTab;
import moe.txuritan.pendulum.common.Reference;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.creativetab.CreativeTabs;

public class CinnabarBlock extends Block {
	public CinnabarBlock(Material material) {
		super(material);
		setCreativeTab(CreativeTab.tabPendulum);
		setBlockName("blockCinnabar");
		setBlockTextureName(Reference.Info.MODID + ":" + getUnlocalizedName());
	}
}
