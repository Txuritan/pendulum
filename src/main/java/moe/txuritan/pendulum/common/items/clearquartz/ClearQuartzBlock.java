package moe.txuritan.pendulum.common.items.clearquartz;

import moe.txuritan.pendulum.client.CreativeTab;
import moe.txuritan.pendulum.common.Reference;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.creativetab.CreativeTabs;

public class ClearQuartzBlock extends Block {
	public ClearQuartzBlock(Material material) {
		super(material);
		setCreativeTab(CreativeTab.tabPendulum);
		setBlockName("blockClearQuartz");
		setBlockTextureName(Reference.Info.MODID + ":" + getUnlocalizedName());
	}
}
