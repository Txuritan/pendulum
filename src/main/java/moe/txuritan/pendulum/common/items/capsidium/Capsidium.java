package moe.txuritan.pendulum.common.items.capsidium;

import cpw.mods.fml.common.registry.GameRegistry;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.item.Item;

public class Capsidium {
	public static Block blockCapsidium = new CapsidiumBlock(Material.iron);
	public static Item ingotCapsidium = new CapsidiumIngot();
	public static Block oreCapsidium = new CapsidiumOre(Material.rock);

	public static void capsidium() {
		GameRegistry.registerBlock(blockCapsidium, "blockCapsidium");
		GameRegistry.registerItem(ingotCapsidium, "ingotCapsidium");
		GameRegistry.registerBlock(oreCapsidium, "oreCapsidium");
	}
}
