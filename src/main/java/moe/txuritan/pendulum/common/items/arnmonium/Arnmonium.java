package moe.txuritan.pendulum.common.items.arnmonium;

import cpw.mods.fml.common.registry.GameRegistry;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.item.Item;

public class Arnmonium {
	public static Block blockArnmonium = new ArnmoniumBlock(Material.iron);
	public static Item ingotArnmonium = new ArnmoniumIngot();
	public static Block oreArnmonium = new ArnmoniumOre(Material.rock);

	public static void arnmonium() {
		GameRegistry.registerBlock(blockArnmonium, "blockArnmonium");
		GameRegistry.registerItem(ingotArnmonium, "ingotArnmonium");
		GameRegistry.registerBlock(oreArnmonium, "oreArnmonium");
	}
}
