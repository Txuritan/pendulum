package moe.txuritan.pendulum.common.items.cassiterite;

import cpw.mods.fml.common.registry.GameRegistry;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.item.Item;

public class Cassiterite {
	public static Block blockCassiterite = new CassiteriteBlock(Material.iron);
	public static Item ingotCassiterite = new CassiteriteIngot();
	public static Block oreCassiterite = new CassiteriteOre(Material.rock);

	public static void cassiterite() {
		GameRegistry.registerBlock(blockCassiterite, "blockCassiterite");
		GameRegistry.registerItem(ingotCassiterite, "ingotCassiterite");
		GameRegistry.registerBlock(oreCassiterite, "oreCassiterite");
	}
}
