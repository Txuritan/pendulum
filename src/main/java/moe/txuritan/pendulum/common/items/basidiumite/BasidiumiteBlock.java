package moe.txuritan.pendulum.common.items.basidiumite;

import moe.txuritan.pendulum.client.CreativeTab;
import moe.txuritan.pendulum.common.Reference;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.creativetab.CreativeTabs;

public class BasidiumiteBlock extends Block {
	public BasidiumiteBlock(Material material) {
		super(material);
		setCreativeTab(CreativeTab.tabPendulum);
		setBlockName("blockBasidiumite");
		setBlockTextureName(Reference.Info.MODID + ":" + getUnlocalizedName());
	}
}
