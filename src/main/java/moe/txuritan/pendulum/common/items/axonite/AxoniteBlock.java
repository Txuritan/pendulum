package moe.txuritan.pendulum.common.items.axonite;

import moe.txuritan.pendulum.client.CreativeTab;
import moe.txuritan.pendulum.common.Reference;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.creativetab.CreativeTabs;

public class AxoniteBlock extends Block {
	public AxoniteBlock(Material material) {
		super(material);
		setCreativeTab(CreativeTab.tabPendulum);
		setBlockName("blockAxonite");
		setBlockTextureName(Reference.Info.MODID + ":" + getUnlocalizedName());
	}
}
