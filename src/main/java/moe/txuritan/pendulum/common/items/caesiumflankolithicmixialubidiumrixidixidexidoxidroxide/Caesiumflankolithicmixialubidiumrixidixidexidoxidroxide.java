package moe.txuritan.pendulum.common.items.caesiumflankolithicmixialubidiumrixidixidexidoxidroxide;

import cpw.mods.fml.common.registry.GameRegistry;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.item.Item;

public class Caesiumflankolithicmixialubidiumrixidixidexidoxidroxide {
	public static Block blockCaesiumflankolithicmixialubidiumrixidixidexidoxidroxide = new CaesiumflankolithicmixialubidiumrixidixidexidoxidroxideBlock(Material.iron);
	public static Item ingotCaesiumflankolithicmixialubidiumrixidixidexidoxidroxide = new CaesiumflankolithicmixialubidiumrixidixidexidoxidroxideIngot();
	public static Block oreCaesiumflankolithicmixialubidiumrixidixidexidoxidroxide = new CaesiumflankolithicmixialubidiumrixidixidexidoxidroxideOre(Material.rock);

	public static void caesiumflankolithicmixialubidiumrixidixidexidoxidroxide() {
		GameRegistry.registerBlock(blockCaesiumflankolithicmixialubidiumrixidixidexidoxidroxide, "blockCaesiumflankolithicmixialubidiumrixidixidexidoxidroxide");
		GameRegistry.registerItem(ingotCaesiumflankolithicmixialubidiumrixidixidexidoxidroxide, "ingotCaesiumflankolithicmixialubidiumrixidixidexidoxidroxide");
		GameRegistry.registerBlock(oreCaesiumflankolithicmixialubidiumrixidixidexidoxidroxide, "oreCaesiumflankolithicmixialubidiumrixidixidexidoxidroxide");
	}
}
