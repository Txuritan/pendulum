package moe.txuritan.pendulum.common.items;

import moe.txuritan.pendulum.common.items.adamantine.Adamantine;
import moe.txuritan.pendulum.common.items.adlourite.Adlourite;
import moe.txuritan.pendulum.common.items.agricite.Agricite;
import moe.txuritan.pendulum.common.items.alkahest.Alkahest;
import moe.txuritan.pendulum.common.items.altarus.Altarus;
import moe.txuritan.pendulum.common.items.alumite.Alumite;
import moe.txuritan.pendulum.common.items.am2.AM2;
import moe.txuritan.pendulum.common.items.arcanite.Arcanite;
import moe.txuritan.pendulum.common.items.arnmonium.Arnmonium;
import moe.txuritan.pendulum.common.items.astmospherium.Astmospherium;
import moe.txuritan.pendulum.common.items.astralsilver.AstralSilver;
import moe.txuritan.pendulum.common.items.axonite.Axonite;
import moe.txuritan.pendulum.common.items.balthazate.Balthazate;
import moe.txuritan.pendulum.common.items.basidiumite.Basidiumite;
import moe.txuritan.pendulum.common.items.bendezium.Bendezium;
import moe.txuritan.pendulum.common.items.bitumen.Bitumen;
import moe.txuritan.pendulum.common.items.bombastium.Bombastium;
import moe.txuritan.pendulum.common.items.buxite.Buxite;
import moe.txuritan.pendulum.common.items.byzantium.Byzantium;
import moe.txuritan.pendulum.common.items.caesiumflankolithicmixialubidiumrixidixidexidoxidroxide.Caesiumflankolithicmixialubidiumrixidixidexidoxidroxide;
import moe.txuritan.pendulum.common.items.capitalium.Capitalium;
import moe.txuritan.pendulum.common.items.capsidium.Capsidium;
import moe.txuritan.pendulum.common.items.carmot.Carmot;
import moe.txuritan.pendulum.common.items.carnelian.Carnelian;
import moe.txuritan.pendulum.common.items.cassiterite.Cassiterite;
import moe.txuritan.pendulum.common.items.cavorite.Cavorite;
import moe.txuritan.pendulum.common.items.ceruclase.Ceruclase;
import moe.txuritan.pendulum.common.items.chlorizite.Chlorizite;
import moe.txuritan.pendulum.common.items.chromium.Chromium;
import moe.txuritan.pendulum.common.items.cinnabar.Cinnabar;
import moe.txuritan.pendulum.common.items.clearquartz.ClearQuartz;
import moe.txuritan.pendulum.common.items.cobalt.Cobalt;
import moe.txuritan.pendulum.common.items.columbite.Columbite;
import moe.txuritan.pendulum.common.items.copper.Copper;
import moe.txuritan.pendulum.common.items.corrodium.Corrodium;
import moe.txuritan.pendulum.common.items.cortex.Cortex;
import moe.txuritan.pendulum.common.items.darkiron.DarkIron;
import moe.txuritan.pendulum.common.items.deepiron.DeepIron;
import moe.txuritan.pendulum.common.items.destronium.Destronium;
import moe.txuritan.pendulum.common.items.disgruntium.Disgruntium;
import moe.txuritan.pendulum.common.items.dragonbane.Dragonbane;
import moe.txuritan.pendulum.common.items.ebony.Ebony;
import moe.txuritan.pendulum.common.items.echsarite.Echsarite;
import moe.txuritan.pendulum.common.items.ekti.Ekti;
import moe.txuritan.pendulum.common.items.elementium.Elementium;
import moe.txuritan.pendulum.common.items.elephantigen.Elephantigen;
import moe.txuritan.pendulum.common.items.elerium.Elerium;
import moe.txuritan.pendulum.common.items.enderium.Enderium;
import moe.txuritan.pendulum.common.items.endurium.Endurium;
import moe.txuritan.pendulum.common.items.essence.Essence;
import moe.txuritan.pendulum.common.items.etherium.Etherium;
import moe.txuritan.pendulum.common.items.exodite.Exodite;
import moe.txuritan.pendulum.common.items.explodium.Explodium;

public class ModBDIR {
	public static void Main() {
		Adamantine.adamantine();
		Adlourite.adlourite();
		Agricite.agricite();
		Alkahest.alkahest();
		Altarus.altarus();
		Alumite.alumite();
		AM2.am2();
		Arcanite.arcanite();
		Arnmonium.arnmonium();
		Astmospherium.astmospherium();
		AstralSilver.astralsilver();
		Axonite.axonite();
		Balthazate.balthazate();
		Basidiumite.basidiumite();
		Bendezium.bendezium();
		Bitumen.bitumen();
		Bombastium.bombastium();
		Buxite.buxite();
		Byzantium.byzantium();
		Caesiumflankolithicmixialubidiumrixidixidexidoxidroxide.caesiumflankolithicmixialubidiumrixidixidexidoxidroxide();
		Capitalium.capitalium();
		Capsidium.capsidium();
		Carmot.carmot();
		Carnelian.carnelian();
		Cassiterite.cassiterite();
		Cavorite.cavorite();
		Ceruclase.ceruclase();
		Chlorizite.chlorizite();
		Chromium.chromium();
		Cinnabar.cinnabar();
		ClearQuartz.clearquartz();
		Cobalt.cobalt();
		Columbite.columbite();
		Copper.copper();
		Corrodium.corrodium();
		Cortex.cortex();
		DarkIron.darkiron();
		DeepIron.deepiron();
		Destronium.destronium();
		Disgruntium.disgruntium();
		Dragonbane.dragonbane();
		Ebony.ebony();
		Echsarite.echsarite();
		Ekti.ekti();
		Elementium.elementium();
		Elephantigen.elephantigen();
		Elerium.elerium();
		Enderium.enderium();
		Endurium.endurium();
		Essence.essence();
		Etherium.etherium();
		Exodite.exodite();
		Explodium.explodium();
	}
	public static void Dictionary() {
		//OreDictionary.registerOre("ingotChromedigizoid", new ItemStack(ModItems.ingotChromedigizoid, 1, 0));
        //OreDictionary.registerOre("oreChromedigizoid", new ItemStack(ModBlocks.oreChromedigizoid, 1, 0));
	}
	public static void Recipes() {
		/* Shapeless Ore Dictionary Recipe
		 * GameRegistry.addRecipe(new ShapelessOreRecipe(new ItemStack(dustBronze, 3), new Object[] { "dustCopper", "dustCopper", "dustTin" }));
		 */
		/* Shaped Ore Dictionary Recipe
		 * GameRegistry.addRecipe(newShapedOreRecipe(Blocks.stone_brick_stairs, "xxx", "yyy", "zzz", 'x', "stone", 'y', Blocks.sponge, 'z', "blockGlass"));
		 */
	}
}
