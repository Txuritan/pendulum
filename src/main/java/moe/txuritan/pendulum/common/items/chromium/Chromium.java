package moe.txuritan.pendulum.common.items.chromium;

import cpw.mods.fml.common.registry.GameRegistry;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.item.Item;

public class Chromium {
	public static Block blockChromium = new ChromiumBlock(Material.iron);
	public static Item ingotChromium = new ChromiumIngot();
	public static Block oreChromium = new ChromiumOre(Material.rock);

	public static void chromium() {
		GameRegistry.registerBlock(blockChromium, "blockChromium");
		GameRegistry.registerItem(ingotChromium, "ingotChromium");
		GameRegistry.registerBlock(oreChromium, "oreChromium");
	}
}
