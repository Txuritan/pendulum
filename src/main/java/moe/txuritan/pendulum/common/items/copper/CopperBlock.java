package moe.txuritan.pendulum.common.items.copper;

import moe.txuritan.pendulum.client.CreativeTab;
import moe.txuritan.pendulum.common.Reference;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.creativetab.CreativeTabs;

public class CopperBlock extends Block {
	public CopperBlock(Material material) {
		super(material);
		setCreativeTab(CreativeTab.tabPendulum);
		setBlockName("blockCopper");
		setBlockTextureName(Reference.Info.MODID + ":" + getUnlocalizedName());
	}
}
