package moe.txuritan.pendulum.common.items.carnelian;

import moe.txuritan.pendulum.client.CreativeTab;
import moe.txuritan.pendulum.common.Reference;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.creativetab.CreativeTabs;

public class CarnelianBlock extends Block {
	public CarnelianBlock(Material material) {
		super(material);
		setCreativeTab(CreativeTab.tabPendulum);
		setBlockName("blockCarnelian");
		setBlockTextureName(Reference.Info.MODID + ":" + getUnlocalizedName());
	}
}
