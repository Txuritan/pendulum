package moe.txuritan.pendulum.common.items.bitumen;

import cpw.mods.fml.common.registry.GameRegistry;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.item.Item;

public class Bitumen {
	public static Block blockBitumen = new BitumenBlock(Material.iron);
	public static Item ingotBitumen = new BitumenIngot();
	public static Block oreBitumen = new BitumenOre(Material.rock);

	public static void bitumen() {
		GameRegistry.registerBlock(blockBitumen, "blockBitumen");
		GameRegistry.registerItem(ingotBitumen, "ingotBitumen");
		GameRegistry.registerBlock(oreBitumen, "oreBitumen");
	}
}
