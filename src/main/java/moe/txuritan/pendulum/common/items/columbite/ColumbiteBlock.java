package moe.txuritan.pendulum.common.items.columbite;

import moe.txuritan.pendulum.client.CreativeTab;
import moe.txuritan.pendulum.common.Reference;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.creativetab.CreativeTabs;

public class ColumbiteBlock extends Block {
	public ColumbiteBlock(Material material) {
		super(material);
		setCreativeTab(CreativeTab.tabPendulum);
		setBlockName("blockColumbite");
		setBlockTextureName(Reference.Info.MODID + ":" + getUnlocalizedName());
	}
}
