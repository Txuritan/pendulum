package moe.txuritan.pendulum.common.items.alumite;

import moe.txuritan.pendulum.client.CreativeTab;
import moe.txuritan.pendulum.common.Reference;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.creativetab.CreativeTabs;

public class AlumiteBlock extends Block {
	public AlumiteBlock(Material material) {
		super(material);
		setCreativeTab(CreativeTab.tabPendulum);
		setBlockName("blockAlumite");
		setBlockTextureName(Reference.Info.MODID + ":" + getUnlocalizedName());
	}
}
