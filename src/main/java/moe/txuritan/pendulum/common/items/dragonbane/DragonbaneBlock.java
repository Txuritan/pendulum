package moe.txuritan.pendulum.common.items.dragonbane;

import moe.txuritan.pendulum.client.CreativeTab;
import moe.txuritan.pendulum.common.Reference;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.creativetab.CreativeTabs;

public class DragonbaneBlock extends Block {
	public DragonbaneBlock(Material material) {
		super(material);
		setCreativeTab(CreativeTab.tabPendulum);
		setBlockName("blockDragonbane");
		setBlockTextureName(Reference.Info.MODID + ":" + getUnlocalizedName());
	}
}
