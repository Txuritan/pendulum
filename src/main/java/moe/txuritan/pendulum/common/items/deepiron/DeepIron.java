package moe.txuritan.pendulum.common.items.deepiron;

import cpw.mods.fml.common.registry.GameRegistry;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.item.Item;

public class DeepIron {
	public static Block blockDeepIron = new DeepIronBlock(Material.iron);
	public static Item ingotDeepIron = new DeepIronIngot();
	public static Block oreDeepIron = new DeepIronOre(Material.rock);

	public static void deepiron() {
		GameRegistry.registerBlock(blockDeepIron, "blockDeepIron");
		GameRegistry.registerItem(ingotDeepIron, "ingotDeepIron");
		GameRegistry.registerBlock(oreDeepIron, "oreDeepIron");
	}
}
