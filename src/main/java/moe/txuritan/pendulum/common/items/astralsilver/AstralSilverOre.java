package moe.txuritan.pendulum.common.items.astralsilver;

import moe.txuritan.pendulum.client.CreativeTab;
import moe.txuritan.pendulum.common.Reference;
import moe.txuritan.pendulum.common.util.StringHelper;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;

import java.util.List;

public class AstralSilverOre extends Block {
	public AstralSilverOre(Material material) {
		super(material);
		setCreativeTab(CreativeTab.tabPendulum);
		setBlockName("oreAstralSilver");
		setBlockTextureName(Reference.Info.MODID + ":" + getUnlocalizedName());
	}
}
