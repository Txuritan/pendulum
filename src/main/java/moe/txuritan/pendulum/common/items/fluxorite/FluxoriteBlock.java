package moe.txuritan.pendulum.common.items.fluxorite;

import moe.txuritan.pendulum.client.CreativeTab;
import moe.txuritan.pendulum.common.Reference;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.creativetab.CreativeTabs;

public class FluxoriteBlock extends Block {
	public FluxoriteBlock(Material material) {
		super(material);
		setCreativeTab(CreativeTab.tabPendulum);
		setBlockName("blockFluxorite");
		setBlockTextureName(Reference.Info.MODID + ":" + getUnlocalizedName());
	}
}
