package moe.txuritan.pendulum.common.items.axonite;

import cpw.mods.fml.common.registry.GameRegistry;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.item.Item;

public class Axonite {
	public static Block blockAxonite = new AxoniteBlock(Material.iron);
	public static Item ingotAxonite = new AxoniteIngot();
	public static Block oreAxonite = new AxoniteOre(Material.rock);

	public static void axonite() {
		GameRegistry.registerBlock(blockAxonite, "blockAxonite");
		GameRegistry.registerItem(ingotAxonite, "ingotAxonite");
		GameRegistry.registerBlock(oreAxonite, "oreAxonite");
	}
}
