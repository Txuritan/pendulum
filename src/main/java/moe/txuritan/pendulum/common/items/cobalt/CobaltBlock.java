package moe.txuritan.pendulum.common.items.cobalt;

import moe.txuritan.pendulum.client.CreativeTab;
import moe.txuritan.pendulum.common.Reference;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.creativetab.CreativeTabs;

public class CobaltBlock extends Block {
	public CobaltBlock(Material material) {
		super(material);
		setCreativeTab(CreativeTab.tabPendulum);
		setBlockName("blockCobalt");
		setBlockTextureName(Reference.Info.MODID + ":" + getUnlocalizedName());
	}
}
