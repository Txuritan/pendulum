package moe.txuritan.pendulum.common.items.essence;

import cpw.mods.fml.common.registry.GameRegistry;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.item.Item;

public class Essence {
	public static Block blockEssence = new EssenceBlock(Material.iron);
	public static Item ingotEssence = new EssenceIngot();
	public static Block oreEssence = new EssenceOre(Material.rock);

	public static void essence() {
		GameRegistry.registerBlock(blockEssence, "blockEssence");
		GameRegistry.registerItem(ingotEssence, "ingotEssence");
		GameRegistry.registerBlock(oreEssence, "oreEssence");
	}
}
