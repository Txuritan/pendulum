package moe.txuritan.pendulum.common.items.copper;

import cpw.mods.fml.common.registry.GameRegistry;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.item.Item;

public class Copper {
	public static Block blockCopper = new CopperBlock(Material.iron);
	public static Item ingotCopper = new CopperIngot();
	public static Block oreCopper = new CopperOre(Material.rock);
	
	public static void copper() {
		GameRegistry.registerBlock(blockCopper, "blockCopper");
		GameRegistry.registerItem(ingotCopper, "ingotCopper");
		GameRegistry.registerBlock(oreCopper, "oreCopper");
	}
}
