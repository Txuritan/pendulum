package moe.txuritan.pendulum.common.items.fulgarator;

import moe.txuritan.pendulum.client.CreativeTab;
import moe.txuritan.pendulum.common.Reference;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.creativetab.CreativeTabs;

public class FulgaratorBlock extends Block {
	public FulgaratorBlock(Material material) {
		super(material);
		setCreativeTab(CreativeTab.tabPendulum);
		setBlockName("blockFulgarator");
		setBlockTextureName(Reference.Info.MODID + ":" + getUnlocalizedName());
	}
}
