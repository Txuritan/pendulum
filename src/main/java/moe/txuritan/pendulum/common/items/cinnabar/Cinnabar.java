package moe.txuritan.pendulum.common.items.cinnabar;

import cpw.mods.fml.common.registry.GameRegistry;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.item.Item;

public class Cinnabar {
	public static Block blockCinnabar = new CinnabarBlock(Material.iron);
	public static Item ingotCinnabar = new CinnabarIngot();
	public static Block oreCinnabar = new CinnabarOre(Material.rock);

	public static void cinnabar() {
		GameRegistry.registerBlock(blockCinnabar, "blockCinnabar");
		GameRegistry.registerItem(ingotCinnabar, "ingotCinnabar");
		GameRegistry.registerBlock(oreCinnabar, "oreCinnabar");
	}
}
