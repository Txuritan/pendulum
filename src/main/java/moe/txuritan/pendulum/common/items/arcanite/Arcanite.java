package moe.txuritan.pendulum.common.items.arcanite;

import cpw.mods.fml.common.registry.GameRegistry;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.item.Item;

public class Arcanite {
	public static Block blockArcanite = new ArcaniteBlock(Material.iron);
	public static Item ingotArcanite = new ArcaniteIngot();
	public static Block oreArcanite = new ArcaniteOre(Material.rock);

	public static void arcanite() {
		GameRegistry.registerBlock(blockArcanite, "blockArcanite");
		GameRegistry.registerItem(ingotArcanite, "ingotArcanite");
		GameRegistry.registerBlock(oreArcanite, "oreArcanite");
	}
}
