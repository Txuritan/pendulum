package moe.txuritan.pendulum.common.items.alumite;

import cpw.mods.fml.common.registry.GameRegistry;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.item.Item;

public class Alumite {
	public static Block blockAlumite = new AlumiteBlock(Material.iron);
	public static Item ingotAlumite = new AlumiteIngot();
	public static Block oreAlumite = new AlumiteOre(Material.rock);

	public static void alumite() {
		GameRegistry.registerBlock(blockAlumite, "blockAlumite");
		GameRegistry.registerItem(ingotAlumite, "ingotAlumite");
		GameRegistry.registerBlock(oreAlumite, "oreAlumite");
	}
}
