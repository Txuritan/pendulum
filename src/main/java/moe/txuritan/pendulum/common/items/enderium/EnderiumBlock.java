package moe.txuritan.pendulum.common.items.enderium;

import moe.txuritan.pendulum.client.CreativeTab;
import moe.txuritan.pendulum.common.Reference;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.creativetab.CreativeTabs;

public class EnderiumBlock extends Block {
	public EnderiumBlock(Material material) {
		super(material);
		setCreativeTab(CreativeTab.tabPendulum);
		setBlockName("blockEnderium");
		setBlockTextureName(Reference.Info.MODID + ":" + getUnlocalizedName());
	}
}
