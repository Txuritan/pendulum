package moe.txuritan.pendulum.common.items.fulgarator;

import cpw.mods.fml.common.registry.GameRegistry;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.item.Item;

public class Fulgarator {
	public static Block blockFulgarator = new FulgaratorBlock(Material.iron);
	public static Item ingotFulgarator = new FulgaratorIngot();
	public static Block oreFulgarator = new FulgaratorOre(Material.rock);

	public static void fulgarator() {
		GameRegistry.registerBlock(blockFulgarator, "blockFulgarator");
		GameRegistry.registerItem(ingotFulgarator, "ingotFulgarator");
		GameRegistry.registerBlock(oreFulgarator, "oreFulgarator");
	}
}
