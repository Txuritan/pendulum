package moe.txuritan.pendulum.common.items.ekti;

import cpw.mods.fml.common.registry.GameRegistry;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.item.Item;

public class Ekti {
	public static Block blockEkti = new EktiBlock(Material.iron);
	public static Item ingotEkti = new EktiIngot();
	public static Block oreEkti = new EktiOre(Material.rock);

	public static void ekti() {
		GameRegistry.registerBlock(blockEkti, "blockEkti");
		GameRegistry.registerItem(ingotEkti, "ingotEkti");
		GameRegistry.registerBlock(oreEkti, "oreEkti");
	}
}
