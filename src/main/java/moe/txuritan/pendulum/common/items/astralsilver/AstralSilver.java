package moe.txuritan.pendulum.common.items.astralsilver;

import cpw.mods.fml.common.registry.GameRegistry;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.item.Item;

public class AstralSilver {
	public static Block blockAstralSilver = new AstralSilverBlock(Material.iron);
	public static Item ingotAstralSilver = new AstralSilverIngot();
	public static Block oreAstralSilver = new AstralSilverOre(Material.rock);

	public static void astralsilver() {
		GameRegistry.registerBlock(blockAstralSilver, "blockAstralSilver");
		GameRegistry.registerItem(ingotAstralSilver, "ingotAstralSilver");
		GameRegistry.registerBlock(oreAstralSilver, "oreAstralSilver");
	}
}
