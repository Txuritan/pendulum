package moe.txuritan.pendulum.common.items.dragonbane;

import cpw.mods.fml.common.registry.GameRegistry;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.item.Item;

public class Dragonbane {
	public static Block blockDragonbane = new DragonbaneBlock(Material.iron);
	public static Item ingotDragonbane = new DragonbaneIngot();
	public static Block oreDragonbane = new DragonbaneOre(Material.rock);

	public static void dragonbane() {
		GameRegistry.registerBlock(blockDragonbane, "blockDragonbane");
		GameRegistry.registerItem(ingotDragonbane, "ingotDragonbane");
		GameRegistry.registerBlock(oreDragonbane, "oreDragonbane");
	}
}
