package moe.txuritan.pendulum.common.items.clearquartz;

import cpw.mods.fml.common.registry.GameRegistry;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.item.Item;

public class ClearQuartz {
	public static Block blockClearQuartz = new ClearQuartzBlock(Material.iron);
	public static Item ingotClearQuartz = new ClearQuartzIngot();
	public static Block oreClearQuartz = new ClearQuartzOre(Material.rock);

	public static void clearquartz() {
		GameRegistry.registerBlock(blockClearQuartz, "blockClearQuartz");
		GameRegistry.registerItem(ingotClearQuartz, "ingotClearQuartz");
		GameRegistry.registerBlock(oreClearQuartz, "oreClearQuartz");
	}
}
