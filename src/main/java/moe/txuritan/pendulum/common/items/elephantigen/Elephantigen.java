package moe.txuritan.pendulum.common.items.elephantigen;

import cpw.mods.fml.common.registry.GameRegistry;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.item.Item;

public class Elephantigen {
	public static Block blockElephantigen = new ElephantigenBlock(Material.iron);
	public static Item ingotElephantigen = new ElephantigenIngot();
	public static Block oreElephantigen = new ElephantigenOre(Material.rock);

	public static void elephantigen() {
		GameRegistry.registerBlock(blockElephantigen, "blockElephantigen");
		GameRegistry.registerItem(ingotElephantigen, "ingotElephantigen");
		GameRegistry.registerBlock(oreElephantigen, "oreElephantigen");
	}
}
