package moe.txuritan.pendulum.common.items.astmospherium;

import cpw.mods.fml.common.registry.GameRegistry;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.item.Item;

public class Astmospherium {
	public static Block blockAstmospherium = new AstmospheriumBlock(Material.iron);
	public static Item ingotAstmospherium = new AstmospheriumIngot();
	public static Block oreAstmospherium = new AstmospheriumOre(Material.rock);

	public static void astmospherium() {
		GameRegistry.registerBlock(blockAstmospherium, "blockAstmospherium");
		GameRegistry.registerItem(ingotAstmospherium, "ingotAstmospherium");
		GameRegistry.registerBlock(oreAstmospherium, "oreAstmospherium");
	}
}
