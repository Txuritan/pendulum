package moe.txuritan.pendulum.common.items.ceruclase;

import cpw.mods.fml.common.registry.GameRegistry;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.item.Item;

public class Ceruclase {
	public static Block blockCeruclase = new CeruclaseBlock(Material.iron);
	public static Item ingotCeruclase = new CeruclaseIngot();
	public static Block oreCeruclase = new CeruclaseOre(Material.rock);

	public static void ceruclase() {
		GameRegistry.registerBlock(blockCeruclase, "blockCeruclase");
		GameRegistry.registerItem(ingotCeruclase, "ingotCeruclase");
		GameRegistry.registerBlock(oreCeruclase, "oreCeruclase");
	}
}
