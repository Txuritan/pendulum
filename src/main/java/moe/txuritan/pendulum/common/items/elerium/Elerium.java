package moe.txuritan.pendulum.common.items.elerium;

import cpw.mods.fml.common.registry.GameRegistry;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.item.Item;

public class Elerium {
	public static Block blockElerium = new EleriumBlock(Material.iron);
	public static Item ingotElerium = new EleriumIngot();
	public static Block oreElerium = new EleriumOre(Material.rock);

	public static void elerium() {
		GameRegistry.registerBlock(blockElerium, "blockElerium");
		GameRegistry.registerItem(ingotElerium, "ingotElerium");
		GameRegistry.registerBlock(oreElerium, "oreElerium");
	}
}
