package moe.txuritan.pendulum.common.items.ebony;

import moe.txuritan.pendulum.client.CreativeTab;
import moe.txuritan.pendulum.common.Reference;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.creativetab.CreativeTabs;

public class EbonyBlock extends Block {
	public EbonyBlock(Material material) {
		super(material);
		setCreativeTab(CreativeTab.tabPendulum);
		setBlockName("blockEbony");
		setBlockTextureName(Reference.Info.MODID + ":" + getUnlocalizedName());
	}
}
