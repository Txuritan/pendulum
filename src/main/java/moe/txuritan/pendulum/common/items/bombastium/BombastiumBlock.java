package moe.txuritan.pendulum.common.items.bombastium;

import moe.txuritan.pendulum.client.CreativeTab;
import moe.txuritan.pendulum.common.Reference;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.creativetab.CreativeTabs;

public class BombastiumBlock extends Block {
	public BombastiumBlock(Material material) {
		super(material);
		setCreativeTab(CreativeTab.tabPendulum);
		setBlockName("blockBombastium");
		setBlockTextureName(Reference.Info.MODID + ":" + getUnlocalizedName());
	}
}
