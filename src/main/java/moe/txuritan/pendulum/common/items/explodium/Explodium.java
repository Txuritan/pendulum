package moe.txuritan.pendulum.common.items.explodium;

import cpw.mods.fml.common.registry.GameRegistry;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.item.Item;

public class Explodium {
	public static Block blockExplodium = new ExplodiumBlock(Material.iron);
	public static Item ingotExplodium = new ExplodiumIngot();
	public static Block oreExplodium = new ExplodiumOre(Material.rock);

	public static void explodium() {
		GameRegistry.registerBlock(blockExplodium, "blockExplodium");
		GameRegistry.registerItem(ingotExplodium, "ingotExplodium");
		GameRegistry.registerBlock(oreExplodium, "oreExplodium");
	}
}
