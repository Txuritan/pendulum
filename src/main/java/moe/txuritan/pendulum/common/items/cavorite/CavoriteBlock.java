package moe.txuritan.pendulum.common.items.cavorite;

import moe.txuritan.pendulum.client.CreativeTab;
import moe.txuritan.pendulum.common.Reference;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.creativetab.CreativeTabs;

public class CavoriteBlock extends Block {
	public CavoriteBlock(Material material) {
		super(material);
		setCreativeTab(CreativeTab.tabPendulum);
		setBlockName("blockCavorite");
		setBlockTextureName(Reference.Info.MODID + ":" + getUnlocalizedName());
	}
}
