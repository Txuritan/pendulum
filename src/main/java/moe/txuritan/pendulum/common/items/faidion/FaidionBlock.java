package moe.txuritan.pendulum.common.items.faidion;

import moe.txuritan.pendulum.client.CreativeTab;
import moe.txuritan.pendulum.common.Reference;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.creativetab.CreativeTabs;

public class FaidionBlock extends Block {
	public FaidionBlock(Material material) {
		super(material);
		setCreativeTab(CreativeTab.tabPendulum);
		setBlockName("blockFaidion");
		setBlockTextureName(Reference.Info.MODID + ":" + getUnlocalizedName());
	}
}
