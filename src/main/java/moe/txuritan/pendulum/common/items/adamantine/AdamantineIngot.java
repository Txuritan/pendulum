package moe.txuritan.pendulum.common.items.adamantine;

import moe.txuritan.pendulum.common.Reference;
import moe.txuritan.pendulum.common.util.StringHelper;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;

import java.util.List;

public class AdamantineIngot extends Item {
	public AdamantineIngot() {
		setMaxStackSize(64);
		setCreativeTab(CreativeTabs.tabMisc);
		setUnlocalizedName("ingotAdamantine");
		setTextureName(Reference.Info.MODID + ":" + getUnlocalizedName());
	}

	public void addInformation(ItemStack stack, EntityPlayer player, List list, boolean par4) {
		if (StringHelper.displayShiftForDetail && !StringHelper.isShiftKeyDown()) {
			list.add(StringHelper.shiftForDetails());
		}
		if (!StringHelper.isShiftKeyDown()) {
			return;
		}
		list.add(StringHelper.getInfoText("info.pendulum.mod.name"));
		list.add(StringHelper.getInfoText("info.pendulum.ingot.adamantine.rarity."));
		list.add(StringHelper.getInfoText("info.pendulum.ingot.adamantine.dim."));
	}
}
