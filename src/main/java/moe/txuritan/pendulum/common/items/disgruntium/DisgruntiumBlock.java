package moe.txuritan.pendulum.common.items.disgruntium;

import moe.txuritan.pendulum.client.CreativeTab;
import moe.txuritan.pendulum.common.Reference;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.creativetab.CreativeTabs;

public class DisgruntiumBlock extends Block {
	public DisgruntiumBlock(Material material) {
		super(material);
		setCreativeTab(CreativeTab.tabPendulum);
		setBlockName("blockDisgruntium");
		setBlockTextureName(Reference.Info.MODID + ":" + getUnlocalizedName());
	}
}
