package moe.txuritan.pendulum.common.items.faidion;

import cpw.mods.fml.common.registry.GameRegistry;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.item.Item;

public class Faidion {
	public static Block blockFaidion = new FaidionBlock(Material.iron);
	public static Item ingotFaidion = new FaidionIngot();
	public static Block oreFaidion = new FaidionOre(Material.rock);

	public static void faidion() {
		GameRegistry.registerBlock(blockFaidion, "blockFaidion");
		GameRegistry.registerItem(ingotFaidion, "ingotFaidion");
		GameRegistry.registerBlock(oreFaidion, "oreFaidion");
	}
}
