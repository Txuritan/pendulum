package moe.txuritan.pendulum.common.items.am2;

import moe.txuritan.pendulum.client.CreativeTab;
import moe.txuritan.pendulum.common.Reference;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.creativetab.CreativeTabs;

public class AM2Block extends Block {
	public AM2Block(Material material) {
		super(material);
		setCreativeTab(CreativeTab.tabPendulum);
		setBlockName("blockAM2");
		setBlockTextureName(Reference.Info.MODID + ":" + getUnlocalizedName());
	}
}
