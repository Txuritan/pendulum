package moe.txuritan.pendulum.common.items.columbite;

import cpw.mods.fml.common.registry.GameRegistry;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.item.Item;

public class Columbite {
	public static Block blockColumbite = new ColumbiteBlock(Material.iron);
	public static Item ingotColumbite = new ColumbiteIngot();
	public static Block oreColumbite = new ColumbiteOre(Material.rock);

	public static void columbite() {
		GameRegistry.registerBlock(blockColumbite, "blockColumbite");
		GameRegistry.registerItem(ingotColumbite, "ingotColumbite");
		GameRegistry.registerBlock(oreColumbite, "oreColumbite");
	}
}
