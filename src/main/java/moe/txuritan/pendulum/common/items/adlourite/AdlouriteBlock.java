package moe.txuritan.pendulum.common.items.adlourite;

import moe.txuritan.pendulum.client.CreativeTab;
import moe.txuritan.pendulum.common.Reference;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.creativetab.CreativeTabs;

public class AdlouriteBlock extends Block {
	public AdlouriteBlock(Material material) {
		super(material);
		setCreativeTab(CreativeTab.tabPendulum);
		setBlockName("blockAdlourite");
		setBlockTextureName(Reference.Info.MODID + ":" + getUnlocalizedName());
	}
}
