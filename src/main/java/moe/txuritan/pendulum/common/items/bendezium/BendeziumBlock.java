package moe.txuritan.pendulum.common.items.bendezium;

import moe.txuritan.pendulum.client.CreativeTab;
import moe.txuritan.pendulum.common.Reference;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.creativetab.CreativeTabs;

public class BendeziumBlock extends Block {
	public BendeziumBlock(Material material) {
		super(material);
		setCreativeTab(CreativeTab.tabPendulum);
		setBlockName("blockBendezium");
		setBlockTextureName(Reference.Info.MODID + ":" + getUnlocalizedName());
	}
}
