package moe.txuritan.pendulum.common.items.elephantigen;

import moe.txuritan.pendulum.client.CreativeTab;
import moe.txuritan.pendulum.common.Reference;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.creativetab.CreativeTabs;

public class ElephantigenBlock extends Block {
	public ElephantigenBlock(Material material) {
		super(material);
		setCreativeTab(CreativeTab.tabPendulum);
		setBlockName("blockElephantigen");
		setBlockTextureName(Reference.Info.MODID + ":" + getUnlocalizedName());
	}
}
