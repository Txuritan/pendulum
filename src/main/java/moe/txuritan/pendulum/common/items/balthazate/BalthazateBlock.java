package moe.txuritan.pendulum.common.items.balthazate;

import moe.txuritan.pendulum.client.CreativeTab;
import moe.txuritan.pendulum.common.Reference;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.creativetab.CreativeTabs;

public class BalthazateBlock extends Block {
	public BalthazateBlock(Material material) {
		super(material);
		setCreativeTab(CreativeTab.tabPendulum);
		setBlockName("blockBalthazate");
		setBlockTextureName(Reference.Info.MODID + ":" + getUnlocalizedName());
	}
}
