package moe.txuritan.pendulum.common.items.capsidium;

import moe.txuritan.pendulum.client.CreativeTab;
import moe.txuritan.pendulum.common.Reference;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.creativetab.CreativeTabs;

public class CapsidiumBlock extends Block {
	public CapsidiumBlock(Material material) {
		super(material);
		setCreativeTab(CreativeTab.tabPendulum);
		setBlockName("blockCapsidium");
		setBlockTextureName(Reference.Info.MODID + ":" + getUnlocalizedName());
	}
}
