package moe.txuritan.pendulum.common.items.elephantigen;

import moe.txuritan.pendulum.client.CreativeTab;
import moe.txuritan.pendulum.common.Reference;
import net.minecraft.entity.Entity;
import net.minecraft.item.ItemArmor;
import net.minecraft.item.ItemStack;

public class ElephantigenArmor extends ItemArmor {
	public String TextureName;

	public ElephantigenArmor(String unlocalizedName, ArmorMaterial material, String textureName, int type) {
		super(material, 0, type);
		textureName = TextureName;
		setUnlocalizedName(unlocalizedName);
		setTextureName(Reference.Info.MODID + ":" + unlocalizedName);
		setCreativeTab(CreativeTab.tabPendulum);
	}
	@Override
	public String getArmorTexture(ItemStack stack, Entity entity, int slot, String type) {
		return Reference.Info.MODID + ":textures /armor/" + this.TextureName + "_" + (this.armorType == 2 ? "2" : "1") + ".png";
	}
}
