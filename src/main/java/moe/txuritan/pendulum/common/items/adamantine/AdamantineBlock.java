package moe.txuritan.pendulum.common.items.adamantine;

import moe.txuritan.pendulum.client.CreativeTab;
import moe.txuritan.pendulum.common.Reference;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.creativetab.CreativeTabs;

public class AdamantineBlock extends Block {
	public AdamantineBlock(Material material) {
		super(material);
		setCreativeTab(CreativeTab.tabPendulum);
		setBlockName("blockAdamantine");
		setBlockTextureName(Reference.Info.MODID + ":" + getUnlocalizedName());
	}
}
