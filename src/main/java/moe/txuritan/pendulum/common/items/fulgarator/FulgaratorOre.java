package moe.txuritan.pendulum.common.items.fulgarator;

import moe.txuritan.pendulum.client.CreativeTab;
import moe.txuritan.pendulum.common.Reference;
import moe.txuritan.pendulum.common.util.StringHelper;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;

import java.util.List;

public class FulgaratorOre extends Block {
	public FulgaratorOre(Material material) {
		super(material);
		setCreativeTab(CreativeTab.tabPendulum);
		setBlockName("oreFulgarator");
		setBlockTextureName(Reference.Info.MODID + ":" + getUnlocalizedName());
	}
}
