package moe.txuritan.pendulum.common.items.capitalium;

import cpw.mods.fml.common.registry.GameRegistry;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.item.Item;

public class Capitalium {
	public static Block blockCapitalium = new CapitaliumBlock(Material.iron);
	public static Item ingotCapitalium = new CapitaliumIngot();
	public static Block oreCapitalium = new CapitaliumOre(Material.rock);

	public static void capitalium() {
		GameRegistry.registerBlock(blockCapitalium, "blockCapitalium");
		GameRegistry.registerItem(ingotCapitalium, "ingotCapitalium");
		GameRegistry.registerBlock(oreCapitalium, "oreCapitalium");
	}
}
