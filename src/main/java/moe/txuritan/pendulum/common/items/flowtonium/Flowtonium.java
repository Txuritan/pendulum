package moe.txuritan.pendulum.common.items.flowtonium;

import cpw.mods.fml.common.registry.GameRegistry;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.item.Item;

public class Flowtonium {
	public static Block blockFlowtonium = new FlowtoniumBlock(Material.iron);
	public static Item ingotFlowtonium = new FlowtoniumIngot();
	public static Block oreFlowtonium = new FlowtoniumOre(Material.rock);

	public static void flowtonium() {
		GameRegistry.registerBlock(blockFlowtonium, "blockFlowtonium");
		GameRegistry.registerItem(ingotFlowtonium, "ingotFlowtonium");
		GameRegistry.registerBlock(oreFlowtonium, "oreFlowtonium");
	}
}
