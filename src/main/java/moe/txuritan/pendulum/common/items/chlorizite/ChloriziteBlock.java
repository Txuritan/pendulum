package moe.txuritan.pendulum.common.items.chlorizite;

import moe.txuritan.pendulum.client.CreativeTab;
import moe.txuritan.pendulum.common.Reference;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.creativetab.CreativeTabs;

public class ChloriziteBlock extends Block {
	public ChloriziteBlock(Material material) {
		super(material);
		setCreativeTab(CreativeTab.tabPendulum);
		setBlockName("blockChlorizite");
		setBlockTextureName(Reference.Info.MODID + ":" + getUnlocalizedName());
	}
}
