package moe.txuritan.pendulum.common.items.fluxorite;

import cpw.mods.fml.common.registry.GameRegistry;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.item.Item;

public class Fluxorite {
	public static Block blockFluxorite = new FluxoriteBlock(Material.iron);
	public static Item ingotFluxorite = new FluxoriteIngot();
	public static Block oreFluxorite = new FluxoriteOre(Material.rock);

	public static void fluxorite() {
		GameRegistry.registerBlock(blockFluxorite, "blockFluxorite");
		GameRegistry.registerItem(ingotFluxorite, "ingotFluxorite");
		GameRegistry.registerBlock(oreFluxorite, "oreFluxorite");
	}
}
