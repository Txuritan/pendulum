package moe.txuritan.pendulum.common.items.ebony;

import cpw.mods.fml.common.registry.GameRegistry;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.item.Item;

public class Ebony {
	public static Block blockEbony = new EbonyBlock(Material.iron);
	public static Item ingotEbony = new EbonyIngot();
	public static Block oreEbony = new EbonyOre(Material.rock);

	public static void ebony() {
		GameRegistry.registerBlock(blockEbony, "blockEbony");
		GameRegistry.registerItem(ingotEbony, "ingotEbony");
		GameRegistry.registerBlock(oreEbony, "oreEbony");
	}
}
