package moe.txuritan.pendulum.common.items.agricite;

import cpw.mods.fml.common.registry.GameRegistry;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.item.Item;

public class Agricite {
	public static Block blockAgricite = new AgriciteBlock(Material.iron);
	public static Item ingotAgricite = new AgriciteIngot();
	public static Block oreAgricite = new AgriciteOre(Material.rock);
	
	public static void agricite() {
		GameRegistry.registerBlock(blockAgricite, "blockAgricite");
		GameRegistry.registerItem(ingotAgricite, "ingotAgricite");
		GameRegistry.registerBlock(oreAgricite, "oreAgricite");
	}
}
