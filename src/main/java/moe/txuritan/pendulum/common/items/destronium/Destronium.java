package moe.txuritan.pendulum.common.items.destronium;

import cpw.mods.fml.common.registry.GameRegistry;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.item.Item;

public class Destronium {
	public static Block blockDestronium = new DestroniumBlock(Material.iron);
	public static Item ingotDestronium = new DestroniumIngot();
	public static Block oreDestronium = new DestroniumOre(Material.rock);

	public static void destronium() {
		GameRegistry.registerBlock(blockDestronium, "blockDestronium");
		GameRegistry.registerItem(ingotDestronium, "ingotDestronium");
		GameRegistry.registerBlock(oreDestronium, "oreDestronium");
	}
}
