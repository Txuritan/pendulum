package moe.txuritan.pendulum.common.items.cavorite;

import cpw.mods.fml.common.registry.GameRegistry;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.item.Item;

public class Cavorite {
	public static Block blockCavorite = new CavoriteBlock(Material.iron);
	public static Item ingotCavorite = new CavoriteIngot();
	public static Block oreCavorite = new CavoriteOre(Material.rock);

	public static void cavorite() {
		GameRegistry.registerBlock(blockCavorite, "blockCavorite");
		GameRegistry.registerItem(ingotCavorite, "ingotCavorite");
		GameRegistry.registerBlock(oreCavorite, "oreCavorite");
	}
}
