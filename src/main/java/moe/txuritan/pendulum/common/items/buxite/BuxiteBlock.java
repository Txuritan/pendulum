package moe.txuritan.pendulum.common.items.buxite;

import moe.txuritan.pendulum.client.CreativeTab;
import moe.txuritan.pendulum.common.Reference;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.creativetab.CreativeTabs;

public class BuxiteBlock extends Block {
	public BuxiteBlock(Material material) {
		super(material);
		setCreativeTab(CreativeTab.tabPendulum);
		setBlockName("blockBuxite");
		setBlockTextureName(Reference.Info.MODID + ":" + getUnlocalizedName());
	}
}
