package moe.txuritan.pendulum.common.items.cortex;

import moe.txuritan.pendulum.client.CreativeTab;
import moe.txuritan.pendulum.common.Reference;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.creativetab.CreativeTabs;

public class CortexBlock extends Block {
	public CortexBlock(Material material) {
		super(material);
		setCreativeTab(CreativeTab.tabPendulum);
		setBlockName("blockCortex");
		setBlockTextureName(Reference.Info.MODID + ":" + getUnlocalizedName());
	}
}
