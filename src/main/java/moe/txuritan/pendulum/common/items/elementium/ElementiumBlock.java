package moe.txuritan.pendulum.common.items.elementium;

import moe.txuritan.pendulum.client.CreativeTab;
import moe.txuritan.pendulum.common.Reference;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.creativetab.CreativeTabs;

public class ElementiumBlock extends Block {
	public ElementiumBlock(Material material) {
		super(material);
		setCreativeTab(CreativeTab.tabPendulum);
		setBlockName("blockElementium");
		setBlockTextureName(Reference.Info.MODID + ":" + getUnlocalizedName());
	}
}
