package moe.txuritan.pendulum.common.items.darkiron;

import moe.txuritan.pendulum.client.CreativeTab;
import moe.txuritan.pendulum.common.Reference;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.creativetab.CreativeTabs;

public class DarkIronBlock extends Block {
	public DarkIronBlock(Material material) {
		super(material);
		setCreativeTab(CreativeTab.tabPendulum);
		setBlockName("blockDarkIron");
		setBlockTextureName(Reference.Info.MODID + ":" + getUnlocalizedName());
	}
}
