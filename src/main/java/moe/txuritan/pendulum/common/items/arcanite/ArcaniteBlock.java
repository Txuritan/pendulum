package moe.txuritan.pendulum.common.items.arcanite;

import moe.txuritan.pendulum.client.CreativeTab;
import moe.txuritan.pendulum.common.Reference;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.creativetab.CreativeTabs;

public class ArcaniteBlock extends Block {
	public ArcaniteBlock(Material material) {
		super(material);
		setCreativeTab(CreativeTab.tabPendulum);
		setBlockName("blockArcanite");
		setBlockTextureName(Reference.Info.MODID + ":" + getUnlocalizedName());
	}
}
