package moe.txuritan.pendulum.common.items.enderium;

import cpw.mods.fml.common.registry.GameRegistry;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.item.Item;

public class Enderium {
	public static Block blockEnderium = new EnderiumBlock(Material.iron);
	public static Item ingotEnderium = new EnderiumIngot();
	public static Block oreEnderium = new EnderiumOre(Material.rock);

	public static void enderium() {
		GameRegistry.registerBlock(blockEnderium, "blockEnderium");
		GameRegistry.registerItem(ingotEnderium, "ingotEnderium");
		GameRegistry.registerBlock(oreEnderium, "oreEnderium");
	}
}
