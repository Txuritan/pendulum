package moe.txuritan.pendulum.common.items.byzantium;

import cpw.mods.fml.common.registry.GameRegistry;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.item.Item;

public class Byzantium {
	public static Block blockByzantium = new ByzantiumBlock(Material.iron);
	public static Item ingotByzantium = new ByzantiumIngot();
	public static Block oreByzantium = new ByzantiumOre(Material.rock);

	public static void byzantium() {
		GameRegistry.registerBlock(blockByzantium, "blockByzantium");
		GameRegistry.registerItem(ingotByzantium, "ingotByzantium");
		GameRegistry.registerBlock(oreByzantium, "oreByzantium");
	}
}
