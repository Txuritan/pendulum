package moe.txuritan.pendulum.common.items.alkahest;

import moe.txuritan.pendulum.client.CreativeTab;
import moe.txuritan.pendulum.common.Reference;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.creativetab.CreativeTabs;

public class AlkahestBlock extends Block {
	public AlkahestBlock(Material material) {
		super(material);
		setCreativeTab(CreativeTab.tabPendulum);
		setBlockName("blockAlkahest");
		setBlockTextureName(Reference.Info.MODID + ":" + getUnlocalizedName());
	}
}
