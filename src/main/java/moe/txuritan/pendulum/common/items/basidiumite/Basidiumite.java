package moe.txuritan.pendulum.common.items.basidiumite;

import cpw.mods.fml.common.registry.GameRegistry;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.item.Item;

public class Basidiumite {
	public static Block blockBasidiumite = new BasidiumiteBlock(Material.iron);
	public static Item ingotBasidiumite = new BasidiumiteIngot();
	public static Block oreBasidiumite = new BasidiumiteOre(Material.rock);

	public static void basidiumite() {
		GameRegistry.registerBlock(blockBasidiumite, "blockBasidiumite");
		GameRegistry.registerItem(ingotBasidiumite, "ingotBasidiumite");
		GameRegistry.registerBlock(oreBasidiumite, "oreBasidiumite");
	}
}
