package moe.txuritan.pendulum.common.items.deepiron;

import moe.txuritan.pendulum.client.CreativeTab;
import moe.txuritan.pendulum.common.Reference;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.creativetab.CreativeTabs;

public class DeepIronBlock extends Block {
	public DeepIronBlock(Material material) {
		super(material);
		setCreativeTab(CreativeTab.tabPendulum);
		setBlockName("blockDeepIron");
		setBlockTextureName(Reference.Info.MODID + ":" + getUnlocalizedName());
	}
}
