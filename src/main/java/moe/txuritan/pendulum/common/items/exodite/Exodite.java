package moe.txuritan.pendulum.common.items.exodite;

import cpw.mods.fml.common.registry.GameRegistry;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.item.Item;

public class Exodite {
	public static Block blockExodite = new ExoditeBlock(Material.iron);
	public static Item ingotExodite = new ExoditeIngot();
	public static Block oreExodite = new ExoditeOre(Material.rock);

	public static void exodite() {
		GameRegistry.registerBlock(blockExodite, "blockExodite");
		GameRegistry.registerItem(ingotExodite, "ingotExodite");
		GameRegistry.registerBlock(oreExodite, "oreExodite");
	}
}
