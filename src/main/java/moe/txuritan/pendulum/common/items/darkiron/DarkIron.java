package moe.txuritan.pendulum.common.items.darkiron;

import cpw.mods.fml.common.registry.GameRegistry;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.item.Item;

public class DarkIron {
	public static Block blockDarkIron = new DarkIronBlock(Material.iron);
	public static Item ingotDarkIron = new DarkIronIngot();
	public static Block oreDarkIron = new DarkIronOre(Material.rock);

	public static void darkiron() {
		GameRegistry.registerBlock(blockDarkIron, "blockDarkIron");
		GameRegistry.registerItem(ingotDarkIron, "ingotDarkIron");
		GameRegistry.registerBlock(oreDarkIron, "oreDarkIron");
	}
}
