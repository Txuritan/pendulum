package moe.txuritan.pendulum.common.items.echsarite;

import moe.txuritan.pendulum.client.CreativeTab;
import moe.txuritan.pendulum.common.Reference;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.creativetab.CreativeTabs;

public class EchsariteBlock extends Block {
	public EchsariteBlock(Material material) {
		super(material);
		setCreativeTab(CreativeTab.tabPendulum);
		setBlockName("blockEchsarite");
		setBlockTextureName(Reference.Info.MODID + ":" + getUnlocalizedName());
	}
}
