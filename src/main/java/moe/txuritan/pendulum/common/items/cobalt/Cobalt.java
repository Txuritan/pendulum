package moe.txuritan.pendulum.common.items.cobalt;

import cpw.mods.fml.common.registry.GameRegistry;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.item.Item;

public class Cobalt {
	public static Block blockCobalt = new CobaltBlock(Material.iron);
	public static Item ingotCobalt = new CobaltIngot();
	public static Block oreCobalt = new CobaltOre(Material.rock);

	public static void cobalt() {
		GameRegistry.registerBlock(blockCobalt, "blockCobalt");
		GameRegistry.registerItem(ingotCobalt, "ingotCobalt");
		GameRegistry.registerBlock(oreCobalt, "oreCobalt");
	}
}
