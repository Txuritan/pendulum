package moe.txuritan.pendulum.common.items.elerium;

import moe.txuritan.pendulum.client.CreativeTab;
import moe.txuritan.pendulum.common.Reference;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.creativetab.CreativeTabs;

public class EleriumBlock extends Block {
	public EleriumBlock(Material material) {
		super(material);
		setCreativeTab(CreativeTab.tabPendulum);
		setBlockName("blockElerium");
		setBlockTextureName(Reference.Info.MODID + ":" + getUnlocalizedName());
	}
}
