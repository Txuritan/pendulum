package moe.txuritan.pendulum.common.items.exodite;

import moe.txuritan.pendulum.client.CreativeTab;
import moe.txuritan.pendulum.common.Reference;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.creativetab.CreativeTabs;

public class ExoditeBlock extends Block {
	public ExoditeBlock(Material material) {
		super(material);
		setCreativeTab(CreativeTab.tabPendulum);
		setBlockName("blockExodite");
		setBlockTextureName(Reference.Info.MODID + ":" + getUnlocalizedName());
	}
}
