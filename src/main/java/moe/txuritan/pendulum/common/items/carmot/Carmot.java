package moe.txuritan.pendulum.common.items.carmot;

import cpw.mods.fml.common.registry.GameRegistry;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.item.Item;

public class Carmot {
	public static Block blockCarmot = new CarmotBlock(Material.iron);
	public static Item ingotCarmot = new CarmotIngot();
	public static Block oreCarmot = new CarmotOre(Material.rock);

	public static void carmot() {
		GameRegistry.registerBlock(blockCarmot, "blockCarmot");
		GameRegistry.registerItem(ingotCarmot, "ingotCarmot");
		GameRegistry.registerBlock(oreCarmot, "oreCarmot");
	}
}
