package moe.txuritan.pendulum.common.items.carnelian;

import cpw.mods.fml.common.registry.GameRegistry;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.item.Item;

public class Carnelian {
	public static Block blockCarnelian = new CarnelianBlock(Material.iron);
	public static Item ingotCarnelian = new CarnelianIngot();
	public static Block oreCarnelian = new CarnelianOre(Material.rock);

	public static void carnelian() {
		GameRegistry.registerBlock(blockCarnelian, "blockCarnelian");
		GameRegistry.registerItem(ingotCarnelian, "ingotCarnelian");
		GameRegistry.registerBlock(oreCarnelian, "oreCarnelian");
	}
}
