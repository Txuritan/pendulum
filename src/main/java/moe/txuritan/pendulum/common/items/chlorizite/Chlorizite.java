package moe.txuritan.pendulum.common.items.chlorizite;

import cpw.mods.fml.common.registry.GameRegistry;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.item.Item;

public class Chlorizite {
	public static Block blockChlorizite = new ChloriziteBlock(Material.iron);
	public static Item ingotChlorizite = new ChloriziteIngot();
	public static Block oreChlorizite = new ChloriziteOre(Material.rock);

	public static void chlorizite() {
		GameRegistry.registerBlock(blockChlorizite, "blockChlorizite");
		GameRegistry.registerItem(ingotChlorizite, "ingotChlorizite");
		GameRegistry.registerBlock(oreChlorizite, "oreChlorizite");
	}
}
