package moe.txuritan.pendulum.common.items.etherium;

import cpw.mods.fml.common.registry.GameRegistry;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.item.Item;

public class Etherium {
	public static Block blockEtherium = new EtheriumBlock(Material.iron);
	public static Item ingotEtherium = new EtheriumIngot();
	public static Block oreEtherium = new EtheriumOre(Material.rock);

	public static void etherium() {
		GameRegistry.registerBlock(blockEtherium, "blockEtherium");
		GameRegistry.registerItem(ingotEtherium, "ingotEtherium");
		GameRegistry.registerBlock(oreEtherium, "oreEtherium");
	}
}
