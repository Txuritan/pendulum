package moe.txuritan.pendulum.common.items.elementium;

import cpw.mods.fml.common.registry.GameRegistry;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.item.Item;

public class Elementium {
	public static Block blockElementium = new ElementiumBlock(Material.iron);
	public static Item ingotElementium = new ElementiumIngot();
	public static Block oreElementium = new ElementiumOre(Material.rock);

	public static void elementium() {
		GameRegistry.registerBlock(blockElementium, "blockElementium");
		GameRegistry.registerItem(ingotElementium, "ingotElementium");
		GameRegistry.registerBlock(oreElementium, "oreElementium");
	}
}
