package moe.txuritan.pendulum.common.items.cortex;

import cpw.mods.fml.common.registry.GameRegistry;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.item.Item;

public class Cortex {
	public static Block blockCortex = new CortexBlock(Material.iron);
	public static Item ingotCortex = new CortexIngot();
	public static Block oreCortex = new CortexOre(Material.rock);

	public static void cortex() {
		GameRegistry.registerBlock(blockCortex, "blockCortex");
		GameRegistry.registerItem(ingotCortex, "ingotCortex");
		GameRegistry.registerBlock(oreCortex, "oreCortex");
	}
}
