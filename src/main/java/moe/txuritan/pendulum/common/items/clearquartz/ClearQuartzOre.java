package moe.txuritan.pendulum.common.items.clearquartz;

import moe.txuritan.pendulum.client.CreativeTab;
import moe.txuritan.pendulum.common.Reference;
import moe.txuritan.pendulum.common.util.StringHelper;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;

import java.util.List;

public class ClearQuartzOre extends Block {
	public ClearQuartzOre(Material material) {
		super(material);
		setCreativeTab(CreativeTab.tabPendulum);
		setBlockName("oreClearQuartz");
		setBlockTextureName(Reference.Info.MODID + ":" + getUnlocalizedName());
	}
}
