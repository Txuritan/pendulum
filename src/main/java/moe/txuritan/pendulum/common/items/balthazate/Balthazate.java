package moe.txuritan.pendulum.common.items.balthazate;

import cpw.mods.fml.common.registry.GameRegistry;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.item.Item;

public class Balthazate {
	public static Block blockBalthazate = new BalthazateBlock(Material.iron);
	public static Item ingotBalthazate = new BalthazateIngot();
	public static Block oreBalthazate = new BalthazateOre(Material.rock);

	public static void balthazate() {
		GameRegistry.registerBlock(blockBalthazate, "blockBalthazate");
		GameRegistry.registerItem(ingotBalthazate, "ingotBalthazate");
		GameRegistry.registerBlock(oreBalthazate, "oreBalthazate");
	}
}
