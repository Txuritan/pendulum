package moe.txuritan.pendulum.common.items.endurium;

import cpw.mods.fml.common.registry.GameRegistry;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.item.Item;

public class Endurium {
	public static Block blockEndurium = new EnduriumBlock(Material.iron);
	public static Item ingotEndurium = new EnduriumIngot();
	public static Block oreEndurium = new EnduriumOre(Material.rock);

	public static void endurium() {
		GameRegistry.registerBlock(blockEndurium, "blockEndurium");
		GameRegistry.registerItem(ingotEndurium, "ingotEndurium");
		GameRegistry.registerBlock(oreEndurium, "oreEndurium");
	}
}
