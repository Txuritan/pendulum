package moe.txuritan.pendulum.common.items.altarus;

import moe.txuritan.pendulum.client.CreativeTab;
import moe.txuritan.pendulum.common.Reference;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.creativetab.CreativeTabs;

public class AltarusBlock extends Block {
	public AltarusBlock(Material material) {
		super(material);
		setCreativeTab(CreativeTab.tabPendulum);
		setBlockName("blockAltarus");
		setBlockTextureName(Reference.Info.MODID + ":" + getUnlocalizedName());
	}
}
