package moe.txuritan.pendulum.common.items.am2;

import cpw.mods.fml.common.registry.GameRegistry;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.item.Item;

public class AM2 {
	public static Block blockAM2 = new AM2Block(Material.iron);
	public static Item ingotAM2 = new AM2Ingot();
	public static Block oreAM2 = new AM2Ore(Material.rock);

	public static void am2() {
		GameRegistry.registerBlock(blockAM2, "blockAM2");
		GameRegistry.registerItem(ingotAM2, "ingotAM2");
		GameRegistry.registerBlock(oreAM2, "oreAM2");
	}
}
