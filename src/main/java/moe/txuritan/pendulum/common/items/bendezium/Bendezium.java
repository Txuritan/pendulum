package moe.txuritan.pendulum.common.items.bendezium;

import cpw.mods.fml.common.registry.GameRegistry;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.item.Item;

public class Bendezium {
	public static Block blockBendezium = new BendeziumBlock(Material.iron);
	public static Item ingotBendezium = new BendeziumIngot();
	public static Block oreBendezium = new BendeziumOre(Material.rock);

	public static void bendezium() {
		GameRegistry.registerBlock(blockBendezium, "blockBendezium");
		GameRegistry.registerItem(ingotBendezium, "ingotBendezium");
		GameRegistry.registerBlock(oreBendezium, "oreBendezium");
	}
}
