package moe.txuritan.pendulum.common.items.echsarite;

import cpw.mods.fml.common.registry.GameRegistry;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.item.Item;

public class Echsarite {
	public static Block blockEchsarite = new EchsariteBlock(Material.iron);
	public static Item ingotEchsarite = new EchsariteIngot();
	public static Block oreEchsarite = new EchsariteOre(Material.rock);

	public static void echsarite() {
		GameRegistry.registerBlock(blockEchsarite, "blockEchsarite");
		GameRegistry.registerItem(ingotEchsarite, "ingotEchsarite");
		GameRegistry.registerBlock(oreEchsarite, "oreEchsarite");
	}
}
