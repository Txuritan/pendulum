package moe.txuritan.pendulum.common.items.adamantine;

import cpw.mods.fml.common.registry.GameRegistry;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.item.Item;

public class Adamantine {
	public static Block blockAdamantine = new AdamantineBlock(Material.iron);
	public static Item ingotAdamantine = new AdamantineIngot();
	public static Block oreAdamantine = new AdamantineOre(Material.rock);
	
	public static void adamantine() {
		GameRegistry.registerBlock(blockAdamantine, "blockAdamantine");
		GameRegistry.registerItem(ingotAdamantine, "ingotAdamantine");
		GameRegistry.registerBlock(oreAdamantine, "oreAdamantine");
	}
}
