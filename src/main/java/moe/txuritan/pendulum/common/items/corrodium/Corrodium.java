package moe.txuritan.pendulum.common.items.corrodium;

import cpw.mods.fml.common.registry.GameRegistry;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.item.Item;

public class Corrodium {
	public static Block blockCorrodium = new CorrodiumBlock(Material.iron);
	public static Item ingotCorrodium = new CorrodiumIngot();
	public static Block oreCorrodium = new CorrodiumOre(Material.rock);

	public static void corrodium() {
		GameRegistry.registerBlock(blockCorrodium, "blockCorrodium");
		GameRegistry.registerItem(ingotCorrodium, "ingotCorrodium");
		GameRegistry.registerBlock(oreCorrodium, "oreCorrodium");
	}
}
