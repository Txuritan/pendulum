package moe.txuritan.pendulum.common.items.agricite;

import moe.txuritan.pendulum.client.CreativeTab;
import moe.txuritan.pendulum.common.Reference;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.creativetab.CreativeTabs;

public class AgriciteBlock extends Block {
	public AgriciteBlock(Material material) {
		super(material);
		setCreativeTab(CreativeTab.tabPendulum);
		setBlockName("blockAgricite");
		setBlockTextureName(Reference.Info.MODID + ":" + getUnlocalizedName());
	}
}
