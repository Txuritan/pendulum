package moe.txuritan.pendulum.common.items.agricite;

import moe.txuritan.pendulum.client.CreativeTab;
import moe.txuritan.pendulum.common.Reference;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemArmor;
import net.minecraft.item.ItemStack;
import net.minecraft.potion.Potion;
import net.minecraft.potion.PotionEffect;
import net.minecraft.world.World;

public class AgriciteArmor extends ItemArmor {
	public String TextureName;

	public AgriciteArmor(String unlocalizedName, ArmorMaterial material, String textureName, int type) {
		super(material, 0, type);
		textureName = TextureName;
		setUnlocalizedName(unlocalizedName);
		setTextureName(Reference.Info.MODID + ":" + unlocalizedName);
		setCreativeTab(CreativeTab.tabPendulum);
	}

	@Override
	public String getArmorTexture(ItemStack stack, Entity entity, int slot, String type) {
		return Reference.Info.MODID + ":textures/armor/" + this.TextureName + "_" + (this.armorType == 2 ? "2" : "1") + ".png";
	}

	@Override
	public void onArmorTick(World world, EntityPlayer player, ItemStack itemStack) {
		/*
		if (itemStack.getItem().equals(ModItems.txuriteLeggings))
			player.addPotionEffect(new PotionEffect(Potion.moveSpeed.id, 40));

		if (itemStack.getItem().equals(ModItems.txuriteChestplate))
			player.addPotionEffect(new PotionEffect(Potion.regeneration.id, 40));
		if (itemStack.getItem().equals(ModItems.yuriteBoots))
			player.addPotionEffect(new PotionEffect(Potion.digSpeed.id, 40));

		if (itemStack.getItem().equals(ModItems.yuriteChestplate))
			player.addPotionEffect(new PotionEffect(Potion.resistance.id, 40));

		if (itemStack.getItem().equals(ModItems.yuriteLeggings))
			player.addPotionEffect(new PotionEffect(Potion.damageBoost.id, 40));
		*/
	}
}