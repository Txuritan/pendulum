package moe.txuritan.pendulum.common.items.corrodium;

import moe.txuritan.pendulum.client.CreativeTab;
import moe.txuritan.pendulum.common.Reference;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.creativetab.CreativeTabs;

public class CorrodiumBlock extends Block {
	public CorrodiumBlock(Material material) {
		super(material);
		setCreativeTab(CreativeTab.tabPendulum);
		setBlockName("blockCorrodium");
		setBlockTextureName(Reference.Info.MODID + ":" + getUnlocalizedName());
	}
}
