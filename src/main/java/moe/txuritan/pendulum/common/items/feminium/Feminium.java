package moe.txuritan.pendulum.common.items.feminium;

import cpw.mods.fml.common.registry.GameRegistry;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.item.Item;

public class Feminium {
	public static Block blockFeminium = new FeminiumBlock(Material.iron);
	public static Item ingotFeminium = new FeminiumIngot();
	public static Block oreFeminium = new FeminiumOre(Material.rock);

	public static void feminium() {
		GameRegistry.registerBlock(blockFeminium, "blockFeminium");
		GameRegistry.registerItem(ingotFeminium, "ingotFeminium");
		GameRegistry.registerBlock(oreFeminium, "oreFeminium");
	}
}
