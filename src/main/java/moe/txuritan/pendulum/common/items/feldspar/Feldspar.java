package moe.txuritan.pendulum.common.items.feldspar;

import cpw.mods.fml.common.registry.GameRegistry;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.item.Item;

public class Feldspar {
	public static Block blockFeldspar = new FeldsparBlock(Material.iron);
	public static Item ingotFeldspar = new FeldsparIngot();
	public static Block oreFeldspar = new FeldsparOre(Material.rock);

	public static void feldspar() {
		GameRegistry.registerBlock(blockFeldspar, "blockFeldspar");
		GameRegistry.registerItem(ingotFeldspar, "ingotFeldspar");
		GameRegistry.registerBlock(oreFeldspar, "oreFeldspar");
	}
}
