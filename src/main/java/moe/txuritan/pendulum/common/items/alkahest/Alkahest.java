package moe.txuritan.pendulum.common.items.alkahest;

import cpw.mods.fml.common.registry.GameRegistry;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.item.Item;

public class Alkahest {
	public static Block blockAlkahest = new AlkahestBlock(Material.iron);
	public static Item ingotAlkahest = new AlkahestIngot();
	public static Block oreAlkahest = new AlkahestOre(Material.rock);

	public static void alkahest() {
		GameRegistry.registerBlock(blockAlkahest, "blockAlkahest");
		GameRegistry.registerItem(ingotAlkahest, "ingotAlkahest");
		GameRegistry.registerBlock(oreAlkahest, "oreAlkahest");
	}
}
