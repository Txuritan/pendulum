package moe.txuritan.pendulum.common;

import cpw.mods.fml.common.event.FMLInitializationEvent;
import cpw.mods.fml.common.event.FMLPostInitializationEvent;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;
import cpw.mods.fml.common.registry.GameRegistry;
import moe.txuritan.pendulum.common.items.ModBDIR;
import moe.txuritan.pendulum.common.world.EndGenerator;
import moe.txuritan.pendulum.common.world.NetherGenerator;
import moe.txuritan.pendulum.common.world.OverworldGenerator;
import net.minecraftforge.common.MinecraftForge;

public class CommonProxy {
	public void preInit(FMLPreInitializationEvent event) {
		ModBDIR.Main();
	}
	public void init(FMLInitializationEvent event) {
		ModBDIR.Dictionary();
		ModBDIR.Recipes();
		GameRegistry.registerWorldGenerator(new OverworldGenerator(), 1);
		GameRegistry.registerWorldGenerator(new NetherGenerator(), 1);
		GameRegistry.registerWorldGenerator(new EndGenerator(), 1);
	}
	public void postInit(FMLPostInitializationEvent event) { 
		
	}
}
