
package moe.txuritan.pendulum.common;

public class Reference {
	public static class Info {
		public static final String MODID = "pendulum";
		public static final String MODNAME = "Pendulum";

		public static final String MCVERSION = "1.7.10";
		public static final String MODVERSION = "0.0.0.1";
		public static final String MODDEBUG = "0.0.0";
		public static final String VERSION = MCVERSION + "_" + MODVERSION + "_" + MODDEBUG;

		public static final String CLIENTPROXY = "moe.txuritan.pendulum.client.ClientProxy";
		public static final String COMMONPROXY = "moe.txuritan.pendulum.common.CommonProxy";

		public static final String DEPENDENCIES = "after:ThermalExpansion;after:ThermalFoundation";
	}
	public static class Resource {
		public static final String RESOURCESPREFIX = Reference.Info.MODID.toLowerCase() + ":";
	}
}
