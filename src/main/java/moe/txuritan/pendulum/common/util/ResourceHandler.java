package moe.txuritan.pendulum.common.util;

import cpw.mods.fml.relauncher.ReflectionHelper;
import moe.txuritan.pendulum.common.Reference;
import net.minecraft.client.Minecraft;
import net.minecraft.client.particle.EffectRenderer;
import net.minecraft.util.ResourceLocation;

import java.util.HashMap;
import java.util.Map;

public class ResourceHandler {
	private static ResourceLocation defaultParticles;
	private static Map<String, ResourceLocation> cachedResources = new HashMap<String, ResourceLocation>();
	public static void bindTexture(ResourceLocation texture) {
		Minecraft.getMinecraft().renderEngine.bindTexture(texture);
	}

	public static void bindDefaultParticles() {
		if (defaultParticles == null) {
			try {
				defaultParticles = (ResourceLocation) ReflectionHelper.getPrivateValue(EffectRenderer.class, null, "particleTextures", "field_110737_b");
			} catch (Exception e) {
			}
		}
		if (defaultParticles != null) bindTexture(defaultParticles);
	}

	public static ResourceLocation getResource(String rs) {
		if (!cachedResources.containsKey(rs))
			cachedResources.put(rs, new ResourceLocation(Reference.Resource.RESOURCESPREFIX + rs));
		return cachedResources.get(rs);
	}

	public static void bindResource(String rs) {
		bindTexture(ResourceHandler.getResource(rs));
	}
}
