﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace FileCreator {
    class Program {
        static void Main(string[] args) {
            string upper = args[0];
            string lower = upper.ToLower();

            System.IO.Directory.CreateDirectory(lower);
            string path = Environment.CurrentDirectory + "\\" + lower + "\\";

            System.Console.WriteLine("Creating Main File");
            string[] mainfile = {
                "package moe.txuritan.pendulum.common.items."+ lower + ";",
                "",
                "import cpw.mods.fml.common.registry.GameRegistry;",
                "import net.minecraft.block.Block;",
                "import net.minecraft.block.material.Material;",
                "import net.minecraft.item.Item;",
                "",
                "public class " + upper + " {",
                "	public static Block block" + upper + " = new " + upper + "Block(Material.iron);",
                "	public static Item ingot" + upper + " = new " + upper + "Ingot();",
                "	public static Block ore" + upper + " = new " + upper + "Ore(Material.rock);",
                "",
                "	public static void " + lower + "() {",
                "		GameRegistry.registerBlock(block" + upper + ", \"block" + upper + "\");",
                "		GameRegistry.registerItem(ingot" + upper + ", \"ingot" + upper + "\");",
                "		GameRegistry.registerBlock(ore" + upper + ", \"ore" + upper + "\");",
                "	}",
                "}"
            };
            System.IO.File.WriteAllLines(path + upper + ".java", mainfile);

            System.Console.WriteLine("Creating Armor File");
            string[] armorfile = {
                "package moe.txuritan.pendulum.common.items."+ lower + ";",
                "",
                "import moe.txuritan.pendulum.client.CreativeTab;",
                "import moe.txuritan.pendulum.common.Reference;",
                "import net.minecraft.entity.Entity;",
                "import net.minecraft.item.ItemArmor;",
                "import net.minecraft.item.ItemStack;",
                "",
                "public class " + upper +"Armor extends ItemArmor {",
                "	public String TextureName;",
                "",
                "	public " + upper +"Armor(String unlocalizedName, ArmorMaterial material, String textureName, int type) {",
                "		super(material, 0, type);",
                "		textureName = TextureName;",
                "		setUnlocalizedName(unlocalizedName);",
                "		setTextureName(Reference.Info.MODID + \":\" + unlocalizedName);",
                "		setCreativeTab(CreativeTab.tabPendulum);",
                "	}",
                "	@Override",
                "	public String getArmorTexture(ItemStack stack, Entity entity, int slot, String type) {",
                "		return Reference.Info.MODID + \":textures /armor/\" + this.TextureName + \"_\" + (this.armorType == 2 ? \"2\" : \"1\") + \".png\";",
                "	}",
                "}"
            };
            System.IO.File.WriteAllLines(path + upper + "Armor.java", armorfile);

            System.Console.WriteLine("Creating Block File");
            string[] blockfile = {
                "package moe.txuritan.pendulum.common.items."+ lower + ";",
                "",
                "import moe.txuritan.pendulum.client.CreativeTab;",
                "import moe.txuritan.pendulum.common.Reference;",
                "import net.minecraft.block.Block;",
                "import net.minecraft.block.material.Material;",
                "import net.minecraft.creativetab.CreativeTabs;",
                "",
                "public class " + upper +"Block extends Block {",
                "	public " + upper +"Block(Material material) {",
                "		super(material);",
                "		setCreativeTab(CreativeTab.tabPendulum);",
                "		setBlockName(\"block" + upper +"\");",
                "		setBlockTextureName(Reference.Info.MODID + \":\" + getUnlocalizedName());",
                "	}",
                "}"
            };
            System.IO.File.WriteAllLines(path + upper + "Block.java", blockfile);

            System.Console.WriteLine("Creating Ingot/Gem File");
            string[] ingotFile = {
                "package moe.txuritan.pendulum.common.items." + lower + ";",
                    "",
                    "import moe.txuritan.pendulum.client.CreativeTab;",
                    "import moe.txuritan.pendulum.common.Reference;",
                    "import moe.txuritan.pendulum.common.util.StringHelper;",
                    "import net.minecraft.creativetab.CreativeTabs;",
                    "import net.minecraft.entity.player.EntityPlayer;",
                    "import net.minecraft.item.Item;",
                    "import net.minecraft.item.ItemStack;",
                    "",
                    "import java.util.List;",
                    "",
                    "public class " + upper + "Ingot extends Item {",
                    "	public " + upper + "Ingot() {",
                    "		setMaxStackSize(64);",
                    "		setCreativeTab(CreativeTab.tabPendulum);",
                    "		setUnlocalizedName(\"ingot" + upper + "\");",
                    "		setTextureName(Reference.Info.MODID + \":\" + getUnlocalizedName());",
					"	}",
					"",
					"	public void addInformation(ItemStack stack, EntityPlayer player, List list, boolean par4) {",
					"		if (StringHelper.displayShiftForDetail && !StringHelper.isShiftKeyDown()) {",
					"			list.add(StringHelper.shiftForDetails());",
					"		}",
					"		if (!StringHelper.isShiftKeyDown()) {",
					"			return;",
					"		}",
                    "		list.add(StringHelper.getInfoText(\"info.pendulum.mod.name\"));",
                    "		list.add(StringHelper.getInfoText(\"info.pendulum.ingot." + lower + ".rarity.\")); ",
                    "		list.add(StringHelper.getInfoText(\"info.pendulum.ingot." + lower + ".dim.\")); ",
                    "	}",
					"}"
            };
            System.IO.File.WriteAllLines(path + upper + "Ingot.java", ingotFile);

            System.Console.WriteLine("Creating Ore File");
            string[] oreFile = {
                "package moe.txuritan.pendulum.common.items." + lower + ";",
                "",
                "import moe.txuritan.pendulum.client.CreativeTab;",
                "import moe.txuritan.pendulum.common.Reference;",
                "import moe.txuritan.pendulum.common.util.StringHelper;",
                "import net.minecraft.block.Block;",
                "import net.minecraft.block.material.Material;",
                "import net.minecraft.creativetab.CreativeTabs;",
                "import net.minecraft.entity.player.EntityPlayer;",
                "import net.minecraft.item.ItemStack;",
                "",
                "import java.util.List;",
                "",
                "public class " + upper + "Ore extends Block {",
                "	public " + upper + "Ore(Material material) {",
                "		super(material);",
                "		setCreativeTab(CreativeTab.tabPendulum);",
                "		setBlockName(\"ore" + upper + "\");",
                "		setBlockTextureName(Reference.Info.MODID + \":\" + getUnlocalizedName());",
				"	}",
				"}"
            };
            System.IO.File.WriteAllLines(path + upper + "Ore.java", oreFile);
        }
    }
}
